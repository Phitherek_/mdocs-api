# frozen_string_literal: true

class Document < ApplicationRecord
  belongs_to :user
  has_one :hierarchy_item
end
