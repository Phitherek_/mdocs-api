# frozen_string_literal: true

class Hierarchy < ApplicationRecord
  belongs_to :user
  has_many :items, class_name: 'HierarchyItem'
end