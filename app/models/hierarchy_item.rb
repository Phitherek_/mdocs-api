# frozen_string_literal: true

class HierarchyItem < ApplicationRecord
  belongs_to :hierarchy
  belongs_to :parent, class_name: 'HierarchyItem'
  belongs_to :document
end
