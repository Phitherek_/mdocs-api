class CreateHierarchyItems < ActiveRecord::Migration[6.0]
  def change
    create_table :hierarchy_items do |t|
      t.string :name
      t.belongs_to :hierarchy
      t.belongs_to :parent
      t.belongs_to :document
      t.timestamps
    end
  end
end
