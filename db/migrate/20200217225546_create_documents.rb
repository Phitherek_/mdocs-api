# frozen_string_literal: true

class CreateDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :documents do |t|
      t.string :title
      t.text :content
      t.string :slug
      t.belongs_to :user
      t.timestamps
    end
  end
end
