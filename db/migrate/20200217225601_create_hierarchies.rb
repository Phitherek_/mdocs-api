# frozen_string_literal: true

class CreateHierarchies < ActiveRecord::Migration[6.0]
  def change
    create_table :hierarchies do |t|
      t.string :name
      t.belongs_to :user
      t.timestamps
    end
  end
end
